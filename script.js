// read more about DOMContentLoaded here: https://developer.mozilla.org/en-US/docs/Web/API/Window/DOMContentLoaded_event
// https://javascript.info/onload-ondomcontentloaded
window.addEventListener("DOMContentLoaded", function () {
  const addContactBtn = document.querySelector("#addContactBtn");
  const cancelButton = document.querySelector("#cancelButton");
  const modalContent = document.querySelector("#modalContent");
  const tableBody = document.querySelector("#tableBody");
  const modal = document.querySelector("#modal");
  const form = document.querySelector("#addContactForm");

  // Application state
  let index = 1;
  let contacts = [];
  // retrieve application state
  retrieveFromStorage();

  // Application logic
  addContactBtn.addEventListener("click", openModal);
  cancelButton.addEventListener("click", closeModal);
  modal.addEventListener("click", function (e) {
    // if click target is outside the modal content
    if (!modalContent.contains(e.target)) {
      closeModal();
    }
  });

  form.addEventListener("submit", function (e) {
    e.preventDefault(); // prevent default behaviour of the form
    // 1. Get value of the input elements
    const fullName = form.querySelector("#fullNameInput").value;
    const phone = form.querySelector("#phoneInput").value;
    const email = form.querySelector("#emailInput").value;
    const company = form.querySelector("#companyInput").value;
    // 2. Append values into the table
    appendNewRow(fullName, phone, email, company);
    // 3. Close the modal
    closeModal();
    // 4. Reset the form
    form.reset();
    // 5. Increment index
    index++;
    // 6. Add to contancts array
    contacts.push({ fullName, phone, email, company });
    // 7. Persist to storage
    persistToStorage();
  });

  // handle delete and edit clicks
  // event phases: https://www.w3.org/TR/DOM-Level-3-Events/#event-flow
  // read more about event delegation at
  // https://javascript.info/event-delegation
  tableBody.addEventListener("click", function (e) {
    if (e.target.matches("button.delete")) {
      deleteRow(e);
    } else if (e.target.matches("button.edit")) {
      editRow(e);
    }
  });

  function openModal() {
    // read more about classlist
    // at: https://developer.mozilla.org/en-US/docs/Web/API/Element/classList
    modal.classList.add("open");
  }

  function closeModal() {
    modal.classList.remove("open");
  }

  function appendNewRow(fullName, phone, email, company) {
    // read more about template literals
    // at MDN: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals
    const row = `
          <tr>
            <td>${index}</td>
            <td>${fullName}</td>
            <td>${phone}</td>
            <td>${email}</td>
            <td>${company}</td>
            <td>
              <button class="edit">Edit</button>
              <button class="delete">Delete</button>
            </td>
          </tr>
    `;
    // read more at: https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML
    tableBody.insertAdjacentHTML("beforeend", row);
  }

  function deleteRow(e) {
    // read more about ChildNode.remove()
    // https://developer.mozilla.org/en-US/docs/Web/API/ChildNode/remove
    const tr = e.target.parentNode.parentNode;
    tr.remove();
    // TODO: remove that contact from local storage too
  }

  function editRow(e) {
    // TODO: Implement edit functionality
  }

  function persistToStorage() {
    // write contacts to local storage
    // and stringify it (array => string)
    // read more at: https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
    localStorage.setItem("contacts", JSON.stringify(contacts));
  }

  function retrieveFromStorage() {
    // read more at: https://www.digitalocean.com/community/tutorials/js-json-parse-stringify
    const storageContacts = localStorage.getItem("contacts");
    // if there are not any contacts in local storage
    // just return (do not do anything)
    if (!storageContacts) {
      return;
    }

    // read contacts from local sotage
    // and parse it (string => array)
    contacts = JSON.parse(storageContacts);
    index = contacts.length; // index should start from array length

    // iterate over contacts array
    // and append each contact to the table
    contacts.forEach(function (contact) {
      appendNewRow(
        contact.fullName,
        contact.phone,
        contact.email,
        contact.company
      );
    });
  }
});
